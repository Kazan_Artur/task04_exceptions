package passive;

import passive.view.MyView;

public class Main {
    public static void main(String[] args) {
       new Domain();
        new MyView().show();
    }
}
