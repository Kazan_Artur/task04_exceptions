package passive;
import passive.flower.*;

import java.util.LinkedList;
import java.util.List;

public class Domain {

  private List<Flower> FlowerList;


  public Domain() {
    FlowerList = new LinkedList<Flower>();

    generateFlowerList();
  }



  public void generateFlowerList() {
    FlowerList.add(new Rose(40));
    FlowerList.add(new Gerbera(30));
    FlowerList.add(new Lilac(20));
    FlowerList.add(new Lotus(25));
  }



  public List<Flower> getFlowerList() {
    return FlowerList;
  }







}
