package passive.view;

@FunctionalInterface
public interface Printable {

  void print();
}
