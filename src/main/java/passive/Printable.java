package passive;

@FunctionalInterface
public interface Printable {

  void print();
}
